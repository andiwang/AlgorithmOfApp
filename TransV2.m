function img2=TransV2(img,x,y,d,angle1)
%definir le centre de matrice originale est (0,0)
%d le distance de le plan de nouvelle image et le camera
%x y definir le position de camera par rapport aux images fixes
base=imread(img); %importer l image
                 %Matrice de l image

s=size(base);
l=s(:,2);
h=s(:,1); 
MatImg=[1 1; l 1; 1 h; l h];
% MatriOriginale=[-l/2 -h/2; l/2 -h/2;-l/2 h/2; l/2 h/2];
% %transformation de Matrice
% tform=fitgeotrans(MatImg ,MatriOriginale, 'affine'); 
% %calcul de nouveau matrice
% img=imwarp(base, tform, 'FillValues',0);
centre=[l/2 h/2];
alpha=atan(x/y);

%deplacement
Dx=x-y*tan(alpha-angle1);
centre=centre+[Dx 0];

m=sqrt(x^2+y^2);
% M=[l/2*(1-cos(alpha)) 1 m-d-l/2*sin(alpha);
%     l/2*(1+cos(alpha)) 1 m-d+l/2*sin(alpha);
%     l/2*(1-cos(alpha)) h m-d-l/2*sin(alpha);
%     l/2*(1+cos(alpha)) h m-d+l/2*sin(alpha)];
M=[centre(:,1)-l/2*cos(alpha-angle1) 1 m-d+(Dx-l/2)*sin(alpha-angle1);
    centre(:,1)+l/2*cos(alpha-angle1) 1 m-d+(Dx+l/2)*sin(alpha-angle1);
    centre(:,1)-l/2*cos(alpha-angle1) h m-d+(Dx-l/2)*sin(alpha-angle1); 
    centre(:,1)+l/2*cos(alpha-angle1) h m-d+(Dx+l/2)*sin(alpha-angle1)]

%
for i=1:4
    for j=1:2
        A(i,j)=d*(M(i,j)-centre(:,j))/(d+M(i,3))+centre(:,j);
    end
end
MatriceNouvelle=A;
%transformation de Matrice
tform=fitgeotrans( MatImg,MatriceNouvelle, 'projective'); 
%calcul de nouveau matrice
img2=imwarp(base, tform, 'FillValues',0);
